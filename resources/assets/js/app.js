/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Router from 'vue-router'
import VeeValidate from 'vee-validate';
import VueProgressBar from 'vue-progressbar'
import router from './router.js'
import Auth from './components/packages/auth/Auth.js'
Vue.component('sidebar', require('./components/Sidebar.vue'));
Vue.use(Auth)
Vue.use(VueAxios, axios, Router, VeeValidate)
Vue.use(Vuetify, {
    theme: {
        primary:'#B71C1C'
         //primary: "#6D4C41", secondary: "#8D6E63", accent: "#FFC107", error: "#f44336", warning: "#ffeb3b", info: "#2196f3", success: "#4caf50"
        // primary: "#6D4C41", secondary: "#8D6E63", accent: "#BF360C", error: "#f44336", warning: "#ffeb3b", info: "#2196f3", success: "#4caf50"
        // primary: "#484848", secondary: "#757575", accent: "#FF1744", error: "#f44336", warning: "#ffeb3b", info: "#2196f3", success: "#4caf50"
    }
})
Vue.use(VueProgressBar, {
    color: 'white',
    failedColor: 'red',
    thickness: '3px'
})
import FullCalendar from "vue-full-calendar";
import "fullcalendar/dist/fullcalendar.min.css";
import "fullcalendar-scheduler/dist/scheduler.min.css";
Vue.use(FullCalendar);
const instance = axios.create();

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {

    // Do something with request error
    return Promise.reject(error);
});

function refreshAppTokens() {
    // Retrieve a new page with a fresh token
    axios.post('crsf')
        .then((token) => {
            console.log(token.data)
            axios.defaults.headers['X-CSRF-TOKEN'] = token.data;
            window.Laravel.csrfToken = token.data;
            document.querySelector('meta[name=csrf-token]').setAttribute('content', token.data);
        });
}
// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    console.log('response')

    return response;
}, function (error) {
    // Do something with response error
    if ([401].indexOf(error.response.status) != -1) {
        Vue.auth.destroyUser();
        router.push('login');
        return;
    }
    if ([419].indexOf(error.response.status) != -1) {
        //location.reload()
        Vue.auth.destroyUser();
        router.push('login');
        refreshAppTokens()
        return
    }

    return Promise.reject(error);
});
export default instance;
Vue.use(Router)
router.beforeEach(
    (to, from, next) => {
        if (!Vue.auth.isAuthenticated() && to.name != "login") {
            next({
                path: '/login'
            })
        } else {
            next();
        }
        return;
    }
)
const app = new Vue({
    el: '#app',
    router,
    created() {
        this.$router.push('login');
    }
});
