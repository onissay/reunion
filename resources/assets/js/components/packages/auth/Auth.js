export default function(Vue){
    Vue.auth={
        setUser(user){
            localStorage.setItem('user', JSON.stringify(user));
        },
        getUser(){
            var user = localStorage.getItem('user');
            if(!user){
                return null;
            }else{
                return JSON.parse(user);
            }
        },
        destroyUser(){
            localStorage.removeItem('user');
        },
        isAuthenticated(){
            if(this.getUser())
                return true;
            return false;
        },
    }
    Object.defineProperties(Vue.prototype,
   {
        $auth: {
        get(){
            return Vue.auth
        }
    }
   })
}