let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js');
   //.sass('resources/assets/sass/app.scss', 'public/css');
mix.webpackConfig({
    resolve: {
        alias: {
            'quasar': path.resolve(`node_modules/quasar-framework/dist/quasar.mat.esm.js`),
            'variables.styl': path.resolve(`node_modules/quasar-framework/dist/core.variables.styl`),
            'quasar-stylus': path.resolve(`node_modules/quasar-framework/dist/quasar.mat.styl`)
        }
    }
})