<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    protected $fillable = ['objet','date','heure','compteRendu','isDone','salle','président','division_id'];
    protected $with = ['divisions','division','guests','files'];
    public function divisions(){
     return $this->belongsToMany('App\Division','reunion_division','reunion_id','division_id');
    }
    public function guests(){
     return $this->belongsToMany('App\Guest','reunion_guest','reunion_id','guest_id');
    }
    public function files(){
     return $this->hasMany('App\File');
    }
    public function division(){
        return $this->belongsTo('App\Division');
    }
    public function setSalleAttribute($value){
        if($this->attributes['président']!='Extérieur')
             $this->attributes['salle'] = $value;
        else {
             $this->attributes['salle'] = null;

        }
    }
}
