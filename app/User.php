<?php

namespace App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom', 'login', 'password','type','division_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $with = ['division'];
    public function division(){
        return $this->belongsTo('App\Division','division_id');
    }
     public function setNomAttribute($value){
        $this->attributes['nom'] = strtoupper($value);
    }
    public function setPrenomAttribute($value){
        $this->attributes['prenom'] = ucwords(strtolower($value));
    }
    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }
    public function salles(){
        return $this->belongsToMany('App\Salle', 'salle_user', 'user_id', 'salle_id');
    }
}
