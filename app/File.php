<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $fillable = ['name','reunion_id'];
    public function getLocation(){
        return $this->attributes['name'];
    }
    public function renunion()
    {
        return $this->belongsToMany('App\Reunion');
    }
    public function isImage()
    {
        $pieces = explode('.', $this->name);
        return in_array($pieces[1], ['gif','png','jpeg','bmp','webp']);
    }
    public function getFileExtension()
    {
        $exploded = explode('.', $this->attributes['name']);
        return $exploded[1];
    }
    public function setNameAttribute($value)
    {
        if ($value==null) {
            return;
        }
        $exploded = explode(',', $value);
        if (isset($this->attributes['name'])) {
            Storage::delete('public/'.$this->attributes['name']);
            $name = $this->attributes['name'];
        } else {
            if (str_contains($exploded[0], 'png')) {
                $extension = 'png';
            } else {
                $extension = 'jpg';
            }

            $name = str_random(16).'.'.$extension;
        }
        $decoded = base64_decode($exploded[1]);
            
        Storage::disk('local')->put('public/'.$name, $decoded);
        $this->attributes['name'] = $name;
    }
    public function getNameAttribute($value)
    {
        if (!$this->attributes['name']) {
            return '';
        }
        return  "data:image/".$this->getFileExtension().";base64,".base64_encode(Storage::get('public/'.$this->attributes['name']));
    }
}
