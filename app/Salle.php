<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
    protected $fillable = ['label'];
    public function users(){
     return $this->belongsToMany('App\User', 'salle_user', 'salle_id', 'user_id');
    }
}
