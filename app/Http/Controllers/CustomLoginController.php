<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class CustomLoginController extends Controller{

    public function authenticate()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }
      public function login(Request $request)
    {
        if(Auth::check())
            return response(['isAuthenticated'=>true]);
        //$credentials = $request->only($this->username(), 'password');
        $authSuccess = Auth::attempt([ 'login'=>$request->input('username'),'password'=>$request->input('password')]);
       // $authSuccess = Auth::attempt([$credentials, $this->username()=>$request->input($this->username())]);
       if($authSuccess) {
            $request->session()->regenerate();
            return response(['success' => true,'user'=>Auth::user(),'entities'=>Auth::user()->entities], Response::HTTP_OK);
        }
        return
            response([
                'success' => false,
                'message' => 'Auth failed (or some other message)'
            ], Response::HTTP_FORBIDDEN);
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return;
    }
    public function crsf(){
        return csrf_token();
    }
}