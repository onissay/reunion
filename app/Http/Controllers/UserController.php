<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\Division;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class UserController extends Controller
{
    
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        return User::all();
        if (Auth::user()) {
        }
        return redirect('login');
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        return view('User.create');
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(Request $request)
    {
        $utilisateur = User::create($request->all());

        return response()->json([
            'message' => 'L\'utilisateur créée avec succès',
            'item' => $utilisateur
        ]);
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function show(User $user)
    {
       return $user;
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function edit($id)
    {
        $User = User::findOrFail($id);
        
        return view('User.edit', compact('User'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        
        return response()->json([
            'message' => 'utilisateur modifiée avec succès',
            'division'=>Division::find($user->division_id)
        ]);
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    *
    * @return Response
    */
    public function destroy(User $user)
    {
       $user->delete();
        return response()->json([
            'message' => 'utilisateur supprimée avec succès',
        ]);
    }
    public function editPWD(Request $request)
    {
        if (Auth::Check()) {
            $request_data = $request->All();
            $user_id = Auth::User()->id;
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make($request_data['password']);
            $obj_user->save();
            return response()->json([
            'message' => 'Mot de passe modifiée avec succès',
        ]);
        } else {
            return redirect()->to('/');
        }
    }
}
