<?php

namespace App\Http\Controllers;

use App\File;
use App\Reunion;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReunionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Reunion::paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reunion = Reunion::create($request->all());
        if ($request->président!='Extérieur') {
            $reunion->guests()->attach($request->guests);
            $reunion->divisions()->attach($request->divisions);
        }
        return ['message'=>'Reunion programmée avec succès','reunion'=>$reunion];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reunion  $reunion
     * @return \Illuminate\Http\Response
     */
    public function show(Reunion $reunion)
    {
        return $reunion;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reunion  $reunion
     * @return \Illuminate\Http\Response
     */
    public function edit(Reunion $reunion)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reunion  $reunion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reunion $reunion)
    {
        $reunion->update($request->all());
        if ($request->président=='Extérieur') {
            $reunion->divisions()->detach();
            $reunion->guests()->detach();
        } else {
            if (!isset($request->divisions[0]['id'])) {
                $reunion->divisions()->sync($request->divisions);
            }
            if (!isset($request->guests[0]['id'])) {
                $reunion->guests()->sync($request->guests);
            }
        }
        
        foreach ($request->input('files') as $file) {
            $file = File::create(['name'=>$file,'reunion_id'=>$reunion->id]);
        }
        return ['message'=>'Reunion modifiée avec succès'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reunion  $reunion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reunion $reunion)
    {
        $files = $reunion->files;
        foreach ($files as $file) {
            Storage::delete('public/'.$file->getLocation());
            $file->delete();
        }
        $reunion->guests()->detach();
        $reunion->divisions()->detach();
        $reunion->delete();
        return ['message'=>'Réunion supprimé avec succès!'];
    }
    public function events($date)
    {
        $date = explode('-', $date);
        return Reunion::whereYear('date', $date[0])
        ->whereMonth('date', $date[1])
        ->get();
    }
    public function pdf(Request $request){
        
        $pdf = PDF::loadView('pdfs.pdf', ['item'=>Reunion::find($request->id)])
       // ->setPaper('A4', 'landscape')
        ->stream();
        return $pdf ;
    }
}
