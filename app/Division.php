<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = ['label','abbr','user_id'];
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
     public function setAbbrAttribute($value){
        $this->attributes['abbr'] = strtoupper($value);
    }
    public function setLabelAttribute($value){
        $this->attributes['label'] = ucwords(strtolower($value));
    }
}
