<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::resource('divisions', 'DivisionController');
Route::resource('users','UserController');
Route::resource('salles','SalleController');
Route::resource('guests','GuestController');
Route::resource('reunions','ReunionController');
Route::get('reunions/events/{date}','ReunionController@events');
Route::post('pdf','ReunionController@pdf');
Route::post('login', 'CustomLoginController@login');
Route::post('logout', 'CustomLoginController@logout');
Route::post('crsf', 'CustomLoginController@crsf');