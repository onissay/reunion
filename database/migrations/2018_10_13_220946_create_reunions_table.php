<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReunionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reunions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('objet')->nullable();
            $table->date('date')->nullable();
            $table->time('heure')->nullable();
            $table->text('compteRendu')->nullable();
            $table->boolean('isDone')->default(false);
            $table->enum('président',["Governeur", "Secrétaire Général", "Chef de division","Extérieur"])->nullable();
            $table->enum('salle',['Salle 1','Salle 2','Salle 3','Bureau du M.Governeur','Bureau du chef de division'])->nullable();
            $table->unsignedInteger('division_id')->nullable();
            $table->foreign('division_id')->references('id')->on('divisions');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reunions');
    }
}
