<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReunionDivision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('reunion_division', function (Blueprint $table) {
            $table->unsignedInteger('division_id')->unsigned();
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->unsignedInteger('reunion_id')->unsigned();
            $table->foreign('reunion_id')->references('id')->on('reunions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reunion_division');
    }
}
