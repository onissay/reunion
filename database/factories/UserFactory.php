<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'nom' => 'Admin',
        'prenom' => 'Admin',
        'login' => 'admin',
        'type' => 'Admin',
        'password' => '$2y$10$fsvSWtmbVz8LcycvI4q8..AJeP6QI7kHrHXk0cuDEURKwx664iB1G',//admin
        //'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
